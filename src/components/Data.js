import axios from 'axios';

export default 
{
    methods: 
    {
        getColors(config) {
            var colors = [];
            config.legend.forEach(function(obj) {
              colors.push(obj.color);
            });
            return colors;
        },

        getLegends(config) {
            var legends = [];
            config.legend.forEach(function(obj) {
              legends.push(obj.label);
            });
            return legends;
        },
        
        sortData() {
          if (this.dataConfigBloc.title) {
            this.title = this.dataConfigBloc.title
          } else if (this.dataBloc.title) {
            this.title = this.dataBloc.title
          } else {
            this.title = null
          }
          this.subtitle = (this.dataConfigBloc.subtitle)? this.dataConfigBloc.subtitle : null;

        var type = (this.dataConfigGraph.type)? this.dataConfigGraph.type : this.dataConfigBloc.type;
        switch (type) {
          case "map":
            this.dataConfigGeneral = this.dataConfigGeneral.map
            this.text = (this.dataBloc.text)? this.dataBloc.text : null;
            this.overlay = (this.dataBloc.map.overlay)? this.dataBloc.map.overlay : null;
            this.lat = (this.dataBloc.map.lat)? this.dataBloc.map.lat : null;
            this.long = (this.dataBloc.map.long) ? this.dataBloc.map.long : null;
            this.zoom_min = (this.dataConfigBloc.zoom.min) ? this.dataConfigBloc.zoom.min : null;
            this.zoom_max = (this.dataConfigBloc.zoom.max) ? this.dataConfigBloc.zoom.max : null;
            this.zoom_default = (this.dataConfigBloc.zoom.default) ? this.dataConfigBloc.zoom.default : null;

            break;
            case "array":
              this.colors = (this.dataConfigGraph && this.dataConfigGraph.legend)? this.getColors(this.dataConfigGraph) : this.getColors(this.dataConfigBloc);
              this.legend = (this.dataConfigGraph && this.dataConfigGraph.legend)? this.getLegends(this.dataConfigGraph) : this.getLegends(this.dataConfigBloc);
              this.legends = (this.dataConfigGraph && this.dataConfigGraph.legend)? this.dataConfigGraph.legend : this.dataConfigBloc.legend;
              this.headers = this.dataConfigGraph.headers ? this.dataConfigGraph.headers : this.dataConfigBloc.headers;
              /*if (this.dataGraph && this.dataConfigGraph.headers) {
                  jQuery.merge(this.dataGraph.datas, this.headers);
              }*/
            break;
            case "chart":
              this.colors = (this.dataConfigGraph && this.dataConfigGraph.legend) ? this.getColors(this.dataConfigGraph) : this.getColors(this.dataConfigBloc);
            break;
        }
        },
        
        async getData()
        {
          try {
            let data = await axios.get('atmo_donnees_31.json');
            let datasBloc = data.data.bloc.find(obj => obj.id == this.blocId);
            if (this.graphId && datasBloc.subBloc) {
              let datasGraph = datasBloc.subBloc.find(obj => obj.id == this.graphId);
              this.dataGraph = datasGraph;
            }
            this.dataBloc = datasBloc;
          } catch (error) {
            console.log(error);
          }

          this.sortData();
        },
        
        async getConfig()
        {
          try {
            let data = await axios.get('config.json');
            let configBloc = data.data.bloc.find(obj => obj.id == this.blocId);
            let configGeneral = data.data.general

            if (this.graphId && configBloc.subBloc) {
              let configGraph = configBloc.subBloc.find(obj => obj.id == this.graphId);
              this.dataConfigGraph = configGraph;
            }
            this.dataConfigBloc = configBloc;
            this.dataConfigGeneral = configGeneral;
          } catch (error) {
            console.log(error);
          }
        }
    },
  
    mounted()
    {
      this.getConfig();
      this.getData();
    }
}