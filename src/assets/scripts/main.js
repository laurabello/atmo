import Vue from 'vue'
import App from '../../App.vue'
import { Icon } from 'vue2-leaflet'
import 'leaflet/dist/leaflet.css'

// Leaflet : 
// this part resolve an issue where the markers would not appear
// delete Icon.Default.prototype._getIconUrl;

// Icon.Default.mergeOptions({
//   iconRetinaUrl: require('vue2-leaflet/dist/images/marker-icon-2x.png'),
//   iconUrl: require('vue2-leaflet/dist/images/marker-icon.png'),
//   shadowUrl: require('vue2-leaflet/dist/images/marker-shadow.png')
// });


window.$ = require('jquery')
window.JQuery = require('jquery')

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')