module.exports = {
  devServer: {
    host: 'atmo.loc',
    port: 8000,
    https: false
  }
}